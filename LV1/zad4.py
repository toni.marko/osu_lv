song_dictionary={}
counter=0
f = open ("song.txt")
for line in f :
    line = line.rstrip ()
    words = line.split ()
    for word in words:
        # Check if the word is already in dictionary
        if word in song_dictionary:
            # Increment count of word by 1
            song_dictionary[word] = song_dictionary[word] + 1
        else:
            # Add the word to dictionary with count 1
            song_dictionary[word] = 1

for key in song_dictionary:
    if (song_dictionary[key]==1):
        counter+=counter
        print(key)

print(song_dictionary)
f.close ()