try:
  num=float(input('Broj: '))
except:
  print('Uneseni znak nije broj')
else:
  if(num<0.0 or num>1.0):
    raise Exception('Broj nije u zadanom rasponu')
  elif(num>=0.9):
    print('A')
  elif(num>=0.8 and num<0.9):
    print('B')
  elif(num>=0.7 and num<0.8):
    print('C')
  elif(num>=0.6 and num<0.7):
    print('D')
  elif(num<0.6):
    print('F')

